#pragma once

#include <iostream>
#include <algorithm>
#include <ctime>
#include <vector>
#include <queue>
#include <string>
#include <fstream>
#include "PriorityQueue.h"
#include "DiGraph.h"
#include "TopologicalSort.h"
#include "SinglyLinkedList.h"

void single_linked_list_test()
{
	SinglyLinkedList<int> myList;
	
	for (int i = 1; i < 20; ++i)
		myList.add(i);

	for (int i = 10; i < 15; ++i)
	{
		auto node = myList.find(i);
		myList.remove(node);
	}

	myList.insert(myList.getHead(), 1001);
	myList.insert(myList.getHead(), 1002);
	myList.insert(myList.getHead(), 1003);
	std::cout << myList;
}


void priority_queue_test()
{
	int elementsNumber = 20;
	std::vector<int> vec(elementsNumber);
	for (int i = 0; i < elementsNumber; ++i)
		vec[i] = i;

	std::random_shuffle(vec.begin(), vec.end());

	PriorityQueue<int> queue;
	std::priority_queue<int> stdQueue;

	for (int i = 0; i < elementsNumber; ++i)
	{

		queue.add(vec[i]);
		stdQueue.push(vec[i]);
	}
	queue.print();

	std::cout << "\n Printing std queue: \n";
	for (int i = 0; i < elementsNumber; ++i)
	{
		std::cout << stdQueue.top() << " ";

		stdQueue.pop();
	}
}

void digraphTest()
{
	// Open file:
	std::ifstream file("..\\Resources\\toplogical_sort_graph.txt");
	if (!file.is_open())
	{
		std::cout << "Cannot open the toplogical_sort_graph.txt file! \n";
		return;
	}

	std::istream& istream = file;//std::cin;
	std::ostream& outStream = std::cout;

	typedef DiGraph<std::string> DigraphType;
	DigraphType digraph;

	int numberOfNodes;
	outStream << "Enter number of nodes: \n";
	istream >> numberOfNodes;

	std::list<DigraphType::Node*> nodes;

	outStream << "Enter the nodes: \n";

	for (int i = 0; i < numberOfNodes; ++i)
	{
		std::string key;
		istream >> key;

		nodes.push_back(digraph.createNode(key));
	}

	std::cout << "Print edges (while printing, q or Q - exit): \n";

	int processedNodes = 0;
	/*while (processedNodes != numberOfNodes)
	{
		outStream << "Enter the adjacent nodes in the format: vertex : adjacentVertex1, adjacentVertex2, ..., adjacentVertexn \n";
		std::string line;
		getline(istream, line);



	}*/
	for (auto node : nodes)
	{
		outStream << "Print adjacent vertices to node " << node->value << " (q or Q stop entering) :\n";
		while (true)
		{
			std::string key;
			istream >> key;

			if (key == "q" || key == "Q")
				break;

			auto otherNode = digraph.getNode(key);
			node->addNode(otherNode);
		}
		

	}

	digraph.print();

	try
	{
		auto sortedNodes = TopologicalSort::sort(digraph);

		outStream << "Sorted Digraph: \n";
		for (auto node : sortedNodes)
			std::cout << node->value << " ";

		outStream << "\n";
	}
	catch (const std::exception& exc)
	{
		outStream << "Error: " << exc.what() << "\n";
	}


	file.close();
}

int main()
{
	std::srand(unsigned(std::time(0)));

	single_linked_list_test();

	system("PAUSE");
	return 0;
}