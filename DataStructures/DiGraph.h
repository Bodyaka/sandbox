#pragma once

#include <list>
#include <vector>
#include <map>
#include <iostream>
#include <algorithm>
#include <utility>

template <class KeyType>
class DiGraph
{
public:
	struct Node
	{
		Node(const KeyType& i_value)
			: value(i_value)
		{	};

		void addNode(Node* node)
		{
			adjacentNodes.push_back(node);
		}

		void removeNode(Node* node)
		{
			auto iter = std::find(adjacentNodes.begin(), adjacentNodes.end(), node);
			if (iter != adjacentNodes.end())
				adjacentNodes.erase(iter);
		}
			
		KeyType value;
		std::list<Node*> adjacentNodes;
	};

	DiGraph();
	~DiGraph();

	Node* createNode(const KeyType& key);

	void registerNode(const KeyType& key, Node* node);
	void unregisterNode(const KeyType& key);

	bool contains(const KeyType& key) const;

	Node* getNode(const KeyType& key) const;

	void print();

	std::vector<Node*> getNodes() const;

private:

	typedef std::map<KeyType, Node*> ContainerType;
	ContainerType m_nodes;
};

template <class KeyType>
DiGraph<KeyType>::DiGraph()
{

}

template <class KeyType>
DiGraph<KeyType>::~DiGraph()
{
	for (auto pair : m_nodes)
		delete pair.second;
}

template <class KeyType>
typename DiGraph<KeyType>::Node* DiGraph<KeyType>::createNode(const KeyType& key)
{
	Node* node = new Node(key);
	registerNode(key, node);
	return node;
}

template <class KeyType>
void DiGraph<KeyType>::registerNode(const KeyType& key, Node* node)
{
	if (contains(key))
		unregisterNode(key);

	m_nodes[key] = node;
}

template <class KeyType>
void DiGraph<KeyType>::unregisterNode(const KeyType& key)
{
	auto iter = m_nodes.find(key);
	if (iter == m_nodes.end())
		return;
	Node* node = iter->second;
	delete node;
	m_nodes.erase(iter);
}

template <class KeyType>
bool DiGraph<KeyType>::contains(const KeyType& key) const
{
	auto iter = m_nodes.find(key);
	return iter != m_nodes.end();
}

template <class KeyType>
typename DiGraph<KeyType>::Node* DiGraph<KeyType>::getNode(const KeyType& key) const
{
	if (!contains(key))
		return nullptr;

	return m_nodes.at(key);
}

template <class KeyType>
void DiGraph<KeyType>::print()
{
	if (m_nodes.empty())
	{
		std::cout << "Digraph is empty!\n";
		return;
	}

	std::for_each(m_nodes.begin(), m_nodes.end(), 
		[=](const std::pair<KeyType, Node*>& value) -> void
		{
			Node* node = value.second;
	
			std::cout << "Node: " << value.first << "; adjacent: ";

			std::list<Node*> adjacentNodes = node->adjacentNodes;
			for (Node* node : adjacentNodes)
				std::cout << node->value << " ";
			std::cout << "\n";
		});
}

template <class KeyType>
std::vector<typename DiGraph<KeyType>::Node*> DiGraph<KeyType>::getNodes() const
{
	std::vector<Node*> nodes;
	nodes.reserve(m_nodes.size());
	for (auto pair : m_nodes)
		nodes.push_back(pair.second);

	return nodes;
}
