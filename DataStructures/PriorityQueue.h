#pragma once

#include <iostream>
#include <functional>

template <class T, class Prioritiezer = std::greater<T>>
class PriorityQueue
{
public:
	PriorityQueue();
	~PriorityQueue();

	void add(const T& element);
	void removeMax();

	const T& getMax() const;

	size_t getSize() const;

	void print();

private:

	size_t getActualSize() const;

	static const size_t s_initialHeapSize = 10;

	T* m_heap;

	size_t m_lastIndex;
	size_t m_heapCapacity;

	Prioritiezer m_comparator;

};

template <class T, class Prioritiezer /*= std::greater<T>*/>
size_t PriorityQueue<T, Prioritiezer>::getActualSize() const
{
	return getSize() + 1;
}

template <class T, class Prioritiezer /*= std::greater<T>*/>
size_t PriorityQueue<T, Prioritiezer>::getSize() const
{
	return m_lastIndex;
}

template <class T, class Prioritiezer /*= std::less<T>*/>
const T& PriorityQueue<T, Prioritiezer>::getMax() const
{
	if (m_lastIndex > 1)
		return m_heap[1];
	else
		return 0;
}



template <class T, class Prioritiezer>
PriorityQueue<T, Prioritiezer>::PriorityQueue()
	: m_heap(new T[s_initialHeapSize])
	, m_lastIndex(0)
	, m_heapCapacity(s_initialHeapSize)
{
}


template <class T, class Prioritiezer>
PriorityQueue<T, Prioritiezer>::~PriorityQueue()
{
	delete[] m_heap;
}

template <class T, class Prioritiezer>
void PriorityQueue<T, Prioritiezer>::add(const T& element)
{
	++m_lastIndex;

	// Extend the array.
	if (getActualSize() > m_heapCapacity)
	{
		size_t oldCapacity = m_heapCapacity;
		m_heapCapacity *= 2;
		T* newHeap = new T[m_heapCapacity];

		for (size_t i = 0; i < getActualSize(); ++i)
			newHeap[i] = m_heap[i];

		//std::copy(m_heap, m_heap + m_heapSize, newHeap);

		delete[] m_heap;

		m_heap = newHeap;
	}

	m_heap[m_lastIndex] = element;

	size_t currentIndex = m_lastIndex;
	// Perform correction of the heap:
	while (true)
	{
		size_t parentIndex = currentIndex / 2;

		if (parentIndex == 0)
			break;

		T& currentElement = m_heap[currentIndex];
		T& parentElement = m_heap[parentIndex];

		if (m_comparator(currentElement, parentElement))
			std::swap(currentElement, parentElement);
		else
			break;
		

		currentIndex = parentIndex;
	}
}


template <class T, class Prioritiezer>
void PriorityQueue<T, Prioritiezer>::removeMax()
{
	if (getSize() == 0)
		return;

	T& topElement = m_heap[1];
	T& lastElement = m_heap[m_lastIndex];

	std::swap(topElement, lastElement);

	--m_lastIndex;

	// Going down:
	if (m_lastIndex > 1)
	{
		size_t currentIndex = 1;
		while (true)
		{
			size_t supremeIndex = currentIndex;
			T& supremeElement = m_heap[supremeIndex];

			size_t leftIndex = 2 * currentIndex;
			size_t rightIndex = leftIndex + 1;

			if (leftIndex < getActualSize())
			{
				T& leftElement = m_heap[leftIndex];
				if (m_comparator(leftElement, supremeElement))
				{
					supremeElement = leftElement;
					supremeIndex = leftIndex;
				}
			}

			if (rightIndex < getActualSize())
			{
				T& rightElement = m_heap[rightIndex];
				if (m_comparator(rightElement, supremeElement))
				{
					supremeElement = rightElement;
					supremeIndex = rightIndex;
				}
			}

			if (supremeIndex != currentIndex)
			{
				std::swap(supremeElement, m_heap[currentIndex]);
				currentIndex = supremeIndex;
			}
			else
				break;
		}
	}
	
}

template <class T, class Prioritiezer>
void PriorityQueue<T, Prioritiezer>::print()
{
	std::cout << "Printing Queue... : ";
	if (getSize() == 0)
		std::cout << "Queue is empty!\n";
	for (size_t i = 1; i < getActualSize(); ++i)
		std::cout << m_heap[i] << " ";
	std::cout << " \n";
}

