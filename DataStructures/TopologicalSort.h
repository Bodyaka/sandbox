#pragma once

#include <vector>
#include "DiGraph.h"

class TopologicalSort
{
public:

	template <class ValueType>
	static std::vector<typename DiGraph<ValueType>::Node*> sort(const DiGraph<ValueType>& graph);

};

template <class ValueType>
static std::vector<typename DiGraph<ValueType>::Node*>
TopologicalSort::sort(const DiGraph<ValueType>& graph)
{
	typedef typename DiGraph<ValueType>::Node NodeType;
	typedef std::vector<NodeType*> NodesContainerType;
	NodesContainerType nodes = graph.getNodes();

	// Calculate indegree of each node:
	std::map<NodeType*, int> nodeIndegrees;

	for (auto node : nodes)
	{
		for (NodeType* adjacentNode : node->adjacentNodes)
			++nodeIndegrees[adjacentNode];
	}

	NodesContainerType sortedNodes;

	// Calculate the nodes with 0 indegree (if no such nodes - graph has cycle):
	NodesContainerType zeroIndegreeNodes;

	for (auto* node : nodes)
	{
		if (nodeIndegrees[node] == 0)
			zeroIndegreeNodes.push_back(node);
	}

	while (sortedNodes.size() != nodes.size()) // while we process all Nodes
	{
		if (zeroIndegreeNodes.empty())
			throw std::exception("ERROR: cannot perform Topological Sorting of the Digraph: The Digraph has cycles!");

		NodesContainerType nextZeroIndegrees;

		for (auto node : zeroIndegreeNodes)
		{
			for (auto adjacentNode : node->adjacentNodes)
			{
				int indegree = --nodeIndegrees[adjacentNode];
				if (indegree == 0)
					nextZeroIndegrees.push_back(adjacentNode);
			}

			sortedNodes.push_back(node);
		}

		zeroIndegreeNodes = nextZeroIndegrees;
	}

	return sortedNodes;
}
