#pragma once

#include <ostream>

template <class ElementType>
class SinglyLinkedList
{

public:
	struct Node
	{
		Node(ElementType i_value, Node* i_next)
			: value(i_value)
			, next(i_next)
		{ }

		ElementType value;
		Node* next;
	};

	SinglyLinkedList();
	~SinglyLinkedList();

	void add(const ElementType& value);
	Node* find(const ElementType& value) const;
	bool remove(Node* node);

	bool insert(Node* node, const ElementType& value);

	Node* getHead() const;
	Node* getTail() const;

private:
	
	Node* m_head;
};

template <class ElementType>
std::ostream& operator << (std::ostream& stream, const SinglyLinkedList<ElementType>& myList)
{
	typename SinglyLinkedList<ElementType>::Node* node = myList.getHead();
	while (node != nullptr)
	{
		stream << node->value << "\n";
		node = node->next;
	}

	return stream;
}

template <class ElementType>
SinglyLinkedList<ElementType>::SinglyLinkedList()
	: m_head(new Node(0, nullptr))
{	

}

template <class ElementType>
SinglyLinkedList<ElementType>::~SinglyLinkedList()
{	
	Node* node = m_head;
	while (node != nullptr)
	{
		Node* currNode = node;
		node = node->next;
		delete currNode;
	}
}

template <class ElementType>
void SinglyLinkedList<ElementType>::add(const ElementType& value)
{
	Node* node = new Node(value, nullptr);

	Node* tail = getTail();
	if (tail == nullptr)
		tail = m_head;

	tail->next = node;
}


template <class ElementType>
typename SinglyLinkedList<ElementType>::Node* SinglyLinkedList<ElementType>::find(const ElementType& value) const
{
	Node* node = getHead();

	while (true)
	{
		if (!node)
			break;

		if (node->value == value)
			return node;

		node = node->next;
	}

	return nullptr;
}

template <class ElementType>
bool SinglyLinkedList<ElementType>::remove(Node* nodeToRemove)
{
	if (!nodeToRemove)
		return false;

	Node* previousNode = m_head;
	Node* node = getHead();

	while (true)
	{
		if (nodeToRemove == node)
		{
			previousNode->next = node->next;
			delete node;
			return true;
		}

		previousNode = node;
		node = node->next;
	}

	return false;
}

template <class ElementType>
bool SinglyLinkedList<ElementType>::insert(Node* beforeNode, const ElementType& value)
{
	Node* previousNode = m_head;
	Node* node = getHead();

	while (true)
	{
		if (node == beforeNode)
		{
			Node* newNode = new Node(value, beforeNode);

			previousNode->next = newNode;
			return true;
		}

		previousNode = node;
		node = node->next;
	}

	return false;
}

template <class ElementType>
typename SinglyLinkedList<ElementType>::Node* SinglyLinkedList<ElementType>::getHead() const
{
	return m_head->next;
}

template <class ElementType>
typename SinglyLinkedList<ElementType>::Node* SinglyLinkedList<ElementType>::getTail() const
{
	// Huge traversal:
	Node* node = getHead();
	while (true)
	{
		if (!node || (node->next == nullptr))
			break;

		node = node->next;
	}
		
	return node;
}


template class SinglyLinkedList<int>;