#pragma once

#include "IProblem.h"

class ArithmeticRangeOverflowCheck
	: public IProblem
{
public:
	ArithmeticRangeOverflowCheck(int number1, int number2);

	virtual void solve();

private:
	int m_number1, m_number2;
};