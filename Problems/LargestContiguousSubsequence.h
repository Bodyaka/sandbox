// Largest Contiguous Subsequence from DP problem in "DS and Algorithms Made EZ".
#pragma once

#include <vector>

class LargestContiguousSubsequence
{
public:

	static const int INVALID_VALUE = -1;

	int solve(int* input, int N);

	static void test();

private:
	int recursiveSolve(int* input, int N, int i, int& maxValue);
	int dynamicProgrammingSolve(int* input, int N, int i, int& maxValue);

	void resetSavedValues(int N);

	std::vector<int> m_savedValues;


};