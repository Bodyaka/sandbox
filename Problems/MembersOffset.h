#pragma once

#include <iostream>
#include "IProblem.h"

class MembersOffset
	: public IProblem
{

	class TempClass
	{
	public:
		int i1;
		char c1;
		int i2;
		char c2;
		float f1;
	};

public:

	virtual void solve()
	{
		int TempClass:: * pToMember1 = &TempClass::i1;
		int TempClass:: * pToMember2 = &TempClass::i2;
		std::cout << "Offsets: \n"
			<< " I1 : " << &pToMember1
			<< " I2: " << &pToMember2 << "\n";
	}
};