#include "SortColors.h"

SortColors::SortColors(std::vector<int>& i_nums)
	: nums(i_nums)
{

}

void SortColors::solve()
{
	int iStartIndex = 0, iEndIndex = nums.size() - 1;

	int iCurrentIndex = 0;
	while (iCurrentIndex <= iEndIndex)
	{
		switch (nums[iCurrentIndex])
		{
		case 0:
			std::swap(nums[iCurrentIndex], nums[iStartIndex]);
			++iCurrentIndex;
			++iStartIndex;
			break;
		case 1:
			++iCurrentIndex;
			break;
		case 2:
			std::swap(nums[iCurrentIndex], nums[iEndIndex]);
			--iEndIndex;
			break;
		}
	}
}

void SortColors::test()
{
	std::vector<int> vec = { 1, 0 };
	SortColors(vec).solve();

	vec = { 2, 0 };
	SortColors(vec).solve();

	vec = { 0,0,0,0,0 };
	SortColors(vec).solve();


	vec = { 1, 1, 1, 1, 1 };
	SortColors(vec).solve();

	vec = { 2, 2, 2, 2, 2 };
	SortColors(vec).solve();

	vec = { 0, 2, 0, 0, 2 };
	SortColors(vec).solve();

	vec = { 1, 0, 1, 0, 1 };
	SortColors(vec).solve();

	vec = { 2, 1, 2, 1, 2 };
	SortColors(vec).solve();

	//assert(vec == {0, 0, 0, 1, 1, 1, 2, 2});
}
