// Problem:  https://www.interviewcake.com/question/java/stock-price

#pragma once

#include <algorithm>
#include <vector>

class StockProfit
{
private:
	int solvePositiveProfit(const std::vector<int>& stockValues);

public:
	int solve(const std::vector<int>& stockValues);

	static void test();
	
};