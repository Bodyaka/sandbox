#include "SwapNodeInPairs.h"

void SwapNodeInPairs::test()
{
	{
		ListNode* nodes1 = solveProblem({ 1, 2, 3, 4 });
		auto vec1 = getValuesVectorFromNodeList(nodes1);

		if (vec1 != std::vector<int>({ 2, 1, 4, 3 }))
			throw std::exception();
	}

	{
		ListNode* nodes = solveProblem({ 1, 2 });
		auto values = getValuesVectorFromNodeList(nodes);

		if (values != std::vector<int>({ 2, 1 }))
			throw std::exception();
	}

	{
		ListNode* nodes = solveProblem({ 1 });
		auto values = getValuesVectorFromNodeList(nodes);

		if (values != std::vector<int>({ 1 }))
			throw std::exception();
	}

	{
		ListNode* nodes = solveProblem({ 1, 2, 3, 4, 5 });
		auto values = getValuesVectorFromNodeList(nodes);

		if (values != std::vector<int>({ 2, 1, 4, 3, 5 }))
			throw std::exception();
	}
}

SwapNodeInPairs::ListNode* SwapNodeInPairs::solveProblem(const std::vector<int>& values)
{
	ListNode* rootNode = createListFromValue(values);
	std::vector<int> checkInputVector = getValuesVectorFromNodeList(rootNode);

	if (values != checkInputVector)
		throw std::exception("Incorrect parsing of input data!");

	return solveImpl(rootNode);
}

SwapNodeInPairs::ListNode* SwapNodeInPairs::createListFromValue(const std::vector<int>& values)
{
	if (values.empty())
		return nullptr;

	ListNode* root = new ListNode(values[0]);
	ListNode* prevNode = root;

	for (size_t i = 1; i < values.size(); ++i)
	{
		ListNode* currentNode = new ListNode(values[i]);
		
		prevNode->next = currentNode;
		prevNode = currentNode;
	}

	return root;
}


std::vector<int> SwapNodeInPairs::getValuesVectorFromNodeList(ListNode* root)
{
	std::vector<int> values;
	while (root != nullptr)
	{
		values.push_back(root->val);
		root = root->next;
	}

	return values;
}

SwapNodeInPairs::ListNode* SwapNodeInPairs::solveImpl(ListNode* rootNode)
{
	if (!rootNode)
		return nullptr;


	ListNode* prevNode = nullptr;
	ListNode* currentNode = rootNode;
	ListNode* newRoot = nullptr;

	while (currentNode != nullptr)
	{
		ListNode* nextNode = currentNode->next;
		if (!nextNode)
			break;

		ListNode* nextNextNode = nextNode->next;
		
		currentNode->next = nextNextNode;
		nextNode->next = currentNode;

		if (prevNode)
			prevNode->next = nextNode;

		prevNode = currentNode;
		currentNode = prevNode->next;

		if (!newRoot)
			newRoot = nextNode;
	}

	return newRoot ? newRoot : rootNode;
}