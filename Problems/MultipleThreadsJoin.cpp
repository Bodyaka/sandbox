#include "MultipleThreadsJoin.h"
#include <iostream>

MultipleThreadsJoin::MultipleThreadsJoin(int operationsCount, int threadsNumber)
	: m_operationsCount(operationsCount)
	, m_threadsNumber(threadsNumber)
	, m_finishedThreads(0)
{
	for (int i = 0; i < threadsNumber; ++i)
	{
		std::thread thread(&MultipleThreadsJoin::performCalculations, this, i + 1);
		m_threads.push_back(std::move(thread));
	}
}

void MultipleThreadsJoin::performCalculations(int ownId)
{
	for (int i = 0; i < m_operationsCount; ++i)
	{
		std::unique_lock<std::mutex> locker(m_writeMutex);

		std::cout << "Thread: " << ownId << "; operation: " << i << "\n";
	}

	std::unique_lock<std::mutex> locker(m_finalResultMutex);
	++m_finishedThreads;
	std::cout << "Thread id = " << ownId << "; Finished threads' value: " << m_finishedThreads << "\n";
}

void MultipleThreadsJoin::solve()
{
	std::cout << "Let's join our threads! \n";
	for (auto& thread : m_threads)
		thread.detach();

	while (!isSolved());
}

bool MultipleThreadsJoin::isSolved() const
{
	return m_finishedThreads == m_threadsNumber;
}
