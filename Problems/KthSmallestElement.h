#include <vector>
#include <limits>

class KthSmallestElementSolution {
public:
	static int kthSmallest(std::vector<std::vector<int>>& matrix, int k) {

		const int N = matrix.size();
		int row_i, row_j, column_i, column_j;

		row_i = 1; row_j = 0;
		column_i = 0; column_j = 1;

		if (k == 1)
			return matrix[0][0];

		const int s_maximum = std::numeric_limits<int>::max();

		for (int i = 1; i < k; ++i)
		{
			int elem_row = isValid(row_i, row_j, N) ? matrix[row_i][row_j] : s_maximum;
			int elem_column = isValid(column_i, column_j, N) ? matrix[column_i][column_j] : s_maximum; 

			int elem = -1;
			if (elem_row <= elem_column)
			{
				++row_i;
				elem = elem_row;
			}
			else
			{
				++column_j;
				elem = elem_column;
			}

			if (i == k - 1)
				return elem;


			if (row_j >= N)
			{
				row_j = column_i + 1;
				++row_i;
			}

			if (column_j >= N)
			{
				column_j = row_j + 1;
				++column_i;
			}
		}

		return 100;
	}

	static bool isValid(int i, int j, const int N);

	static void test();
};