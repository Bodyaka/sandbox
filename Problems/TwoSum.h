#pragma once

#include <vector>
#include <iostream>
#include "IProblem.h"


class TwoSum
	: public IProblem
{
public:
	TwoSum(const std::vector<int> & numbers, int desiredNumber);

	void solve() override;

private:
	std::vector<int> m_numbers;
	int m_desiredNumber;

};