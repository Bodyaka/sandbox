#include <iostream>
#include <cassert>
#include <queue>
#include <algorithm>
#include <numeric>
#include "WorldLadder.h"


class LadderItem
{
public:
	LadderItem()
	: wordIndex(-1)
	, ladderSize(-1)
	, rootItem(nullptr)
	{

	}

	size_t wordIndex;
	size_t ladderSize;

	LadderItem* rootItem;
	std::vector<LadderItem*> adjacentItems;

};




WorldLadder::WorldLadder(const std::string& startWord, const std::string& endWord, const std::vector<std::string>& possibleWords)
	: m_startWord(startWord)
	, m_endWord(endWord)
	, m_possibleWords(possibleWords)
{
	auto newEnd = std::unique(m_possibleWords.begin(), m_possibleWords.end());
	m_possibleWords = std::vector<std::string>(m_possibleWords.begin(), newEnd);

	auto endWordIter = std::find(m_possibleWords.begin(), m_possibleWords.end(), endWord);
	if (endWordIter != m_possibleWords.end())
		m_possibleWords.erase(endWordIter);

	auto startWordIter = std::find(m_possibleWords.begin(), m_possibleWords.end(), startWord);
	if (startWordIter != m_possibleWords.end())
		m_possibleWords.erase(startWordIter);

}

WorldLadder::~WorldLadder()
{

}

void WorldLadder::solve()
{
	try
	{
		// Create the array of indices in order to save some space:
		std::vector<size_t> wordIndices;
		wordIndices.resize(m_possibleWords.size());

		std::iota(wordIndices.begin(), wordIndices.end(), 0);

		std::vector<size_t> originalWordIndices = wordIndices;

		std::queue<size_t> elementsToCheck;
		elementsToCheck.push(END_WORD_INDEX);

		ElementItem rootItem(END_WORD_INDEX, 0);
		m_closestElementsDict[END_WORD_INDEX] = rootItem;

		while (!elementsToCheck.empty())
		{
			auto elementIndex = elementsToCheck.front();
			elementsToCheck.pop();

			const std::string& desiredWord = getString(elementIndex);

			auto closestElements = getClosestElementsToWord(desiredWord, wordIndices);


			size_t ladderSize = m_closestElementsDict[elementIndex].ladderSize + 1;
			for (auto index : closestElements)
			{
				ElementItem item(elementIndex, ladderSize);
				elementsToCheck.push(index);
				m_closestElementsDict[index] = item;
			}
		}

		auto closestElementsToStartWord = getClosestElementsToWord(m_startWord, originalWordIndices);

		size_t lowestLadderSize = std::numeric_limits<size_t>::max();
		size_t closestIndexWithLowestLadderSize = INVALID_INDEX;

		for (auto index : closestElementsToStartWord)
		{
			size_t ladderSize = m_closestElementsDict[index].ladderSize;
			if (ladderSize < lowestLadderSize)
			{
				lowestLadderSize = ladderSize;
				closestIndexWithLowestLadderSize = index;
			}
		}


		if (closestIndexWithLowestLadderSize == INVALID_INDEX)
			throw std::exception("No!");

		std::vector<std::string> wordLadder;
		wordLadder.push_back(m_startWord);

		size_t currentIndex = closestIndexWithLowestLadderSize;
		while (true)
		{
			wordLadder.push_back(getString(currentIndex));

			if (currentIndex == END_WORD_INDEX)
				break;

			currentIndex = m_closestElementsDict[currentIndex].rootIndex;
		}

		std::cout << "Final word ladder route: \n";
		for (const auto& str : wordLadder)
		{
			std::cout << str << " ";
		}
		std::cout << "\n";

	}
	catch (const std::exception& exc)
	{
		std::cout << "Couldn't find the closest path from word: " << m_startWord << " to word: " << m_endWord << "\n";
	}


}

size_t WorldLadder::calculatePermutations(const std::string& word1, const std::string& word2) const
{
	assert(word1.size() == word2.size());

	size_t numberOfPermutations = 0;
	for (size_t i = 0; i < word1.size(); ++i)
	{
		if (word1[i] != word2[i])
			++numberOfPermutations;
	}

	return numberOfPermutations;
}

const std::string& WorldLadder::getString(LadderItem* item) const
{
	return getString(item->wordIndex);
}

const std::string& WorldLadder::getString(size_t index) const
{
	if (index == END_WORD_INDEX)
		return m_endWord;

	return m_possibleWords.at(index);
}

std::vector<size_t> WorldLadder::getClosestElementsToWord(const std::string& desiredWord, std::vector<size_t>& indices) const
{
	typedef std::pair<size_t, size_t> IntermediateItemType;

	std::vector<IntermediateItemType> intermediateItems;
	intermediateItems.reserve(indices.size());


	for (auto index : indices)
	{
		IntermediateItemType item;
		item.first = index;
		item.second = calculatePermutations(getString(index), desiredWord);

		intermediateItems.push_back(item);
	}


	std::sort(intermediateItems.begin(), intermediateItems.end(), 
		[=](const IntermediateItemType& rhs, IntermediateItemType& lhs) -> bool
	{
		return rhs.second < lhs.second;
	});


	if (intermediateItems.empty())
		return std::vector<size_t>();

	auto lowestNumberOfPermutations = intermediateItems.front().second;
	if (lowestNumberOfPermutations > 1)
		throw std::exception("Input data is incorrect!");

	indices.clear();
	std::vector<size_t> closestElements;
	for (auto item : intermediateItems)
	{
		if (item.second > lowestNumberOfPermutations)
			indices.push_back(item.first);
		else
			closestElements.push_back(item.first);
	}

	return closestElements;
}
