#pragma once

#include <iostream>

#include <cstdlib>
#include "FizzBuzz.h"
#include "AtoiConverter.h"
#include "StringReversal.h"
#include "EndianessTest.h"
#include "OneBitsCalculator.h"
#include "MultipleThreadsJoin.h"
#include "CStringTest.h"
#include "UnsignedIntCalculator.h"
#include "ArithmeticRangeOverflowCheck.h"
#include "MembersOffset.h"
#include "CStringReversal.h"
#include "TwoSum.h"
#include "SortedArrayDuplicates.h"
#include "TriangleProblem.h"
#include "WorldLadder.h"

#include "BinaryTreeSerializer.h"

#include "KthSmallestElement.h"
#include "StockProfit.h"
#include "UniqueDigitsProblem.h"
#include "LargestContiguousSubsequence.h"
#include "Quicksort4.h"
#include "ValidParenthesis.h"
#include "SearchInsertPosition.h"
#include "SortColors.h"
#include "LongestUniqueSubstring.h"
#include "IntegerSerializer.h"

#include "BadVersion.h"
#include "SwapNodeInPairs.h"

#include <bitset>

void test_cstring_reversal()
{
	char c_str[] = "";
	CStringReversal str(c_str);
	str.solve();
}

void testTwoSumProblem()
{
	std::vector<int> numbers = { -1, -2, -3, -4, -5};
	TwoSum problem(numbers, -8);
	problem.solve();
}

int main()
{
	SwapNodeInPairs::test();
	BinaryTreeSerializer::test();

	assert(IntegerSerializer::test(4));
	assert(IntegerSerializer::test(125));
	assert(IntegerSerializer::test(-79));
	assert(IntegerSerializer::test(127));
	assert(IntegerSerializer::test(-128));
	assert(IntegerSerializer::test(-324567));
	assert(IntegerSerializer::test(124567));
	assert(IntegerSerializer::test(0));

	return 0;
	LongestUniqueSubstring::test();
	SortColors::test();
	SearchInsertPosition::test();
	ValidParenthesis::test();
	return 0;
	BadVersion::test();
	Quicksort4::test();
	LargestContiguousSubsequence::test();
	StockProfit::test();
	KthSmallestElementSolution::test();
	UniqueDigitsProblem::test();
	std::vector<std::string> wordList = { "hot", "dot", "dog", "lot", "log" };

	WorldLadder ladderProblem("hit", "cog", wordList);
	ladderProblem.solve();

	system("PAUSE");
	return 0;
/*
	std::cout << "size of the biggest type: " << sizeof(unsigned long long) << "\n";
	while (true)
	{
		int value;
		std::cin >> value;

		std::bitset<sizeof(int) * 8> bits(value);
		std::string negativeNumber = bits.to_string();
		std::cout << "Binary representation is : " << negativeNumber << "\n";
	}
	*/
	/*IProblem* problem = new OneBitsCalculator(-157);
	problem->solve();*/
	//IProblem* mtProblem = new MultipleThreadsJoin(15, 3);
	//mtProblem->solve();

	/*CStringTest* tester = new CStringTest();
	tester->solve();*/

	SortedArrayDuplicates::defaultTest();

	//testTwoSumProblem();
	/*ArithmeticRangeOverflowCheck checker(0x7FFFFFFF, 1);
	checker.solve();

	MembersOffset meberOffsets;
	meberOffsets.solve();

	test_cstring_reversal();*/

	std::vector<std::vector<int>> in;
	in.push_back({ -1 });
	in.push_back({ 2, 3 });
	in.push_back({ 1, -1, -3 });
	//in.push_back({ 4, 1, 8, 3 });

	//in.push_back({ -2 });


	TriangleProblem problem(in);
	problem.solve();
	int result = problem.getResult();

	system("PAUSE");

	return 0;
}