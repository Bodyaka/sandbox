#pragma once
#include <string>
#include <limits>

class IntegerSerializer
{
public:
	static bool test(int value)
	{
		auto str = serializeInteger(value);
		return deserializeInteger(str, 0) == value;
	}

private:

	static std::string serializeInteger(int value) {
		std::string str;

		int mask = ~((~0) << 8);
		for (int i = 0; i < INT_SIZE; ++i) {
			char portion = (value & mask) >> i*8;
			str += char(portion);

			mask <<= 8;
		}

		return str;
	}

	union CHAR_BYTE_DATA
	{
		signed char s_char;
		unsigned char u_char;
	};

	static int deserializeInteger(const std::string& str, int currentIndex) {

		int value = 0;
		for (int i = 0; i < INT_SIZE; ++i) {
			CHAR_BYTE_DATA byte;
			byte.s_char = str[currentIndex + i];
			int temp = byte.u_char << i*8;
			value |= temp;
		}

		return value;
	}
	static const int INT_SIZE = sizeof(int);
};