// https://leetcode.com/problems/add-two-numbers-ii/
// This is not correct implementation - it doesn't pass huge int tests =(
#pragma once

#include "IProblem.h"

class AddTwoNumbers2
	: public IProblem
{
public:
	struct ListNode {
	    int val;
	    ListNode *next;
	    ListNode(int x) : val(x), next(NULL) {}
	};

	AddTwoNumbers2(ListNode* l1, ListNode* l2);

	virtual void solve() override;
	ListNode* getResult() const;

private:

	typedef int long_t;
	ListNode* addTwoNumbers(ListNode* l1, ListNode* l2);
	long_t convertListNodeToInt(ListNode* l);
	ListNode* convertIntToListNode(long_t value);

private:
	ListNode* m_list1,
			* m_list2,
			* m_result;
};