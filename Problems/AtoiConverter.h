#pragma once

#include "IProblem.h"
#include <string>

class AtoiConverter
	: public IProblem
{
public:
	AtoiConverter();
	AtoiConverter(const std::string& str);

	void solve() override;
private:
	std::string m_str;
};