#include "AtoiConverter.h"

#include <iostream>

static int is_digit(char ch)
{
	static const int ASCII_ZERO_ID = 48;
	static const int ASCII_NINE_ID = 57;

	if (ch >= ASCII_ZERO_ID && ch <= ASCII_NINE_ID)
		return ch - ASCII_ZERO_ID;

	return -1;
}

AtoiConverter::AtoiConverter()
{
	while (true)
	{
		std::cout << "Enter the string (q or Q - exit): \n";

		std::cin >> m_str;

		if (m_str == std::string("q") || m_str == std::string("Q"))
			break;

		solve();
	}
}

AtoiConverter::AtoiConverter(const std::string& str)
	: IProblem()
	, m_str(str)
{

}

void AtoiConverter::solve()
{
	int result = 0;

	int startIndex = 0;
	bool toNegate = false;

	if (m_str[0] == '-')
	{
		toNegate = true;
		++startIndex;
	}

	for (size_t i = startIndex; i < m_str.size(); ++i)
	{
		char ch = m_str[i];
		int digit = is_digit(ch);

		if (digit == -1)
		{
			std::cout << "Error: input string cannot be converted to the integral value! \n";
			return;
		}

		result = result * 10 + digit;
	}

	if (toNegate)
		result *= -1;

	int stdResult = atoi(m_str.c_str());
	std::cout << "Custom Atoi : " << result << " C++ result = " << stdResult << "\n";
}
