#include "SortedArrayDuplicates.h"
#include <iostream>
#include <algorithm>

SortedArrayDuplicates::SortedArrayDuplicates(std::vector<int>& numbers)
: m_numbers(numbers)
{

}

void SortedArrayDuplicates::solve()
{
	std::cout << "Input array: \n";
	std::for_each(m_numbers.begin(), m_numbers.end(), [=](int value) {	std::cout << value << " "; });
	std::cout << "\n";

	auto iter = m_numbers.begin();
	auto fixedIter = m_numbers.begin();
	if (fixedIter == m_numbers.end())
		return;

	++iter;
	auto placeHolder = iter;


	while (iter != m_numbers.end())
	{
		if (*iter != *fixedIter)
		{
			fixedIter = iter;
			*placeHolder = *fixedIter;
			++placeHolder;
		}
		
		++iter;
	}

	m_numbers.erase(placeHolder, m_numbers.end());

	std::cout << "Removed duplicates \n";
	std::for_each(m_numbers.begin(), m_numbers.end(), [=](int value) {	std::cout << value << " "; });
}

void SortedArrayDuplicates::defaultTest()
{
	//std::vector<int> numbers = { 1, 1, 2, 2, 3, 4, 5, 6, 7, 7, 7 };
	std::vector<int> numbers = { 1, 1, 1, 1 };
	SortedArrayDuplicates problem(numbers);
	problem.solve();
}
