#pragma once
#include <cassert>
// Forward declaration of isBadVersion API.
bool isBadVersion(int version)
{
	return version >= 3;
}

class BadVersion {
public:
	int firstBadVersion(int n) {

		return recursiveFind(n, 0, n);
	}

	static void test()
	{
		BadVersion solver;
		auto find1 = solver.firstBadVersion(4);
		auto find2 = solver.firstBadVersion(8);
		auto find3 = solver.firstBadVersion(9);
		auto find4 = solver.firstBadVersion(1100);

		int toFind = 3;
		assert(find1 == toFind);
		assert(find2 == toFind);
		assert(find3 == toFind);
		assert(find4 == toFind);
	}
private:
	int recursiveFind(int n, int low, int high)
	{
		if (low >= high) {
			return low;
			/*
			int index = std::max(std::min(high, low), 0);
			bool bPrevious = isBadVersion(index);
			if (bPrevious) {
				for (; index >= 0; --index) {
					if (!isBadVersion(index))
						break;
				}

				return ++index;
			}
			else {
				for (; index < n; ++index) {
					if (isBadVersion(index))
						break;
				}

				return index;
			}*/
		}
		int mid = low + (high - low) / 2;

		if (isBadVersion(mid))
			return recursiveFind(n, low, mid);
		else
			return recursiveFind(n, mid + 1, high);
	}


};