// https://leetcode.com/problems/serialize-and-deserialize-bst/

#pragma once

#include <string>

/**
* Definition for a binary tree node.
* struct TreeNode {
*     int val;
*     TreeNode *left;
*     TreeNode *right;
*     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
* };
*/
class BinaryTreeSerializer {
public:

	struct TreeNode {
		int val;
		TreeNode *left;
		TreeNode *right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	// Encodes a tree to a single string.
	std::string serialize(TreeNode* root); 

	// Decodes your encoded data to tree.
	TreeNode* deserialize(std::string data);

	static void test();

private:

	int serializeNode(TreeNode* node, std::string& str, int currentDepth);
	int deserializeNode(TreeNode* parentNode, const std::string& str, int& currentIndex, int currentDepth);

	std::string serializeInteger(int value) {
		std::string str;

		int mask = ~((~0) << 8);
		for (int i = 0; i < INT_SIZE; ++i) {
			char portion = (value & mask) >> i * 8;
			str += char(portion);

			mask <<= 8;
		}

		return str;
	}

	union CHAR_BYTE_DATA
	{
		signed char s_char;
		unsigned char u_char;
	};

	int deserializeInteger(const std::string& str, int& currentIndex) {

		int value = 0;
		for (int i = 0; i < INT_SIZE; ++i) {
			CHAR_BYTE_DATA byte;
			byte.s_char = str[currentIndex + i];
			int temp = byte.u_char << i * 8;
			value |= temp;
		}

		currentIndex += INT_SIZE;
		return value;
	}
	static const int INT_SIZE = sizeof(int);


	

};

// Your Codec object will be instantiated and called as such:
// Codec codec;
// codec.deserialize(codec.serialize(root));