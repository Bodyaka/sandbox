#pragma once

#include <vector>

class Quicksort4
{
public:
	Quicksort4();

	void solve();

	static void test();

private:
	int insertionSort(const std::vector<int>& sourceVec) const;
	int quickSort(const std::vector<int>& sourceVec) const;

	void quickSortHelper(std::vector<int>& vec, int lo, int high, int& numberOfSwaps) const;
	int quickSortPartition(std::vector<int>& vec, int lo, int high, int& numberOfSwaps) const;
};