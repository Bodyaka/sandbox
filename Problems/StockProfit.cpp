#include "StockProfit.h"
#include <cassert>

int StockProfit::solve(const std::vector<int>& stockValues)
{
	if (stockValues.size() < 2)
		return 0;

	int localMin = stockValues[0];
	int localMax = stockValues[1];

	int globalProfit = stockValues[1] - stockValues[0];
	for (int i = 1; i < stockValues.size(); ++i)
	{
		int stockValue = stockValues[i];
		if (stockValue < localMin)
		{
			localMin = localMax = stockValue;
		}
		else if (stockValue > localMax)
		{
			localMax = stockValue;
			int localProfit = localMax - localMin;

			globalProfit = std::max(localProfit, globalProfit);
		}
	}

	return globalProfit;
}

void StockProfit::test()
{
	{
		std::vector<int> values = { 10, 7, 5, 8, 11, 9 };
		StockProfit solver;
		auto answer = solver.solve(values);

		assert(answer == 6);
	}

	{
		std::vector<int> values = { 10, 7, 5, 8, 11, 9, 2, 9, 3, 10, 1 };
		StockProfit solver;
		auto answer = solver.solve(values);

		assert(answer == 8);
	}

	{
		std::vector<int> values = {  };
		StockProfit solver;
		auto answer = solver.solve(values);

		assert(answer == 0);
	}

	{
		std::vector<int> values = {10};
		StockProfit solver;
		auto answer = solver.solve(values);

		assert(answer == 0);
	}

	{
		std::vector<int> values = { 10, 11 };
		StockProfit solver;
		auto answer = solver.solve(values);

		assert(answer == 1);
	}

	{
		std::vector<int> values = { 11, 1 };
		StockProfit solver;
		auto answer = solver.solve(values);

		assert(answer == -10);
	}


	{
		std::vector<int> values = { 11, 10, 7, 5, 1, -100 };
		StockProfit solver;
		auto answer = solver.solve(values);

		assert(answer == -1);
	}

}

int StockProfit::solvePositiveProfit(const std::vector<int>& stockValues)
{
	if (stockValues.empty())
		return 0;

	int localMin = stockValues[0];
	int localMax = localMin;

	int globalProfit = 0;
	for (int i = 1; i < stockValues.size(); ++i)
	{
		int stockValue = stockValues[i];
		if (stockValue < localMin)
		{
			localMin = localMax = stockValue;
		}
		else if (stockValue > localMax)
		{
			localMax = stockValue;
			int localProfit = localMax - localMin;

			globalProfit = std::max(localProfit, globalProfit);
		}
	}

	return globalProfit;
}
