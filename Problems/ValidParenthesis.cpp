#include "ValidParenthesis.h"

void ValidParenthesis::solve()
{
	auto vec1 = generateParenthesis(0);
	assert(vec1.empty());
	auto vec2 = generateParenthesis(1);
	assert(vec2.size() == 1 && vec2[0] == "()");

	auto vec4 = generateParenthesis(3);
	auto findAndAssert = [](const std::vector<std::string>& result, const std::string& expectedResult) -> bool {
		auto iter = std::find(result.begin(), result.end(), expectedResult);
		return iter != result.end();
	};

	assert(vec4.size() == 5);
	assert(findAndAssert(vec4, "((()))"));
	assert(findAndAssert(vec4, "(()())"));
	assert(findAndAssert(vec4, "(())()"));
	assert(findAndAssert(vec4, "()(())"));
	assert(findAndAssert(vec4, "()()()"));

}

std::vector<std::string> ValidParenthesis::generateParenthesis(int n) {

	if (n == 0)
		return std::vector<std::string>();

	std::vector<std::string> validStrings;

	const int expectedSize = n * 2;
	std::string tempString;
	tempString.resize(expectedSize);

	tempString[0] = '(';
	backtraceFunc(validStrings, tempString, 1, 0, 1, expectedSize);

	return validStrings;
}

void ValidParenthesis::backtraceFunc(std::vector<std::string>& validStrings, std::string& currentString, int openBrackets, int closedBrackets, int currentIndex, const int n) {
	if (openBrackets > n / 2 || closedBrackets > n / 2 || closedBrackets > openBrackets)
		return;

	if (currentIndex == n) {
		if (openBrackets == closedBrackets)
			validStrings.push_back(currentString);

		return;
	}

	currentString[currentIndex] = '(';
	backtraceFunc(validStrings, currentString, openBrackets + 1, closedBrackets, currentIndex + 1, n);

	currentString[currentIndex] = ')';
	backtraceFunc(validStrings, currentString, openBrackets, closedBrackets + 1, currentIndex + 1, n);
}

void ValidParenthesis::test()
{
	ValidParenthesis solver;
	solver.solve();
}
