// https://leetcode.com/problems/sort-characters-by-frequency/
#pragma once

#include "IProblem.h"

class CharFrequencySorter
	: public IProblem
{
public:
	CharFrequencySorter(const std::string& i_s);

	virtual void solve() override;
	std::string getResult() const { return m_resultString; }

private:
	void mySolution();
	void hashBacketSolution();


	std::string m_string,
				m_resultString;
};