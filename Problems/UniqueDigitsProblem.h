#pragma once

class UniqueDigitsProblem
{
public:
	static int solve(int power);

	static void test();
};