#pragma once

#include "IProblem.h"

class EndianessTest
	: public IProblem
{
public:
	EndianessTest();

	void solve() override;
};