#include "BinaryTreeSerializer.h"
#include <cassert>

std::string BinaryTreeSerializer::serialize(TreeNode* root) {

	if (!root) {
		return std::string();
	}

	std::string str;
	serializeNode(root, str, 0);
	return str;
}

BinaryTreeSerializer::TreeNode* BinaryTreeSerializer::deserialize(std::string data)
{

	if (data.empty()) {
		return nullptr;
	}


	int currentIndex = 0;
	int rootValue = deserializeInteger(data, currentIndex);
	TreeNode* node = new TreeNode(rootValue);
	deserializeNode(node, data, currentIndex, 0);

	return node;
}

int BinaryTreeSerializer::serializeNode(TreeNode* node, std::string& str, int currentDepth)
{
	if (!node)
		return -1;

	str += serializeInteger(node->val);

	int nullDepth = -1;
	if (node->left) {
		str += 'l';
		nullDepth = serializeNode(node->left, str, currentDepth + 1);
	}

	if (node->right) {
		if (nullDepth != -1) {
			str += 'b';
			str += serializeInteger(nullDepth - currentDepth);
		}

		str += 'r';
		return serializeNode(node->right, str, currentDepth + 1);
	}

	if (!node->left && !node->right) {
		return currentDepth;
	}

	return nullDepth;
}

int BinaryTreeSerializer::deserializeNode(TreeNode* parentNode, const std::string& str, int& currentIndex, int currentDepth)
{
	if (currentIndex >= str.size()) {
		return -1;
	}

	char ch = str[currentIndex++];
	int value = deserializeInteger(str, currentIndex);

	int distance = -1;
	if (ch == 'l') {
		auto node = new TreeNode(value);
		parentNode->left = node;
		distance = deserializeNode(node, str, currentIndex, currentDepth + 1);
	}
	else if (ch == 'r') {
		auto node = new TreeNode(value);
		parentNode->right = node;
		distance = deserializeNode(node, str, currentIndex, currentDepth + 1);
	}
	else if (ch == 'b') {
		return value - 1;
	}

	if (distance == 0) {
		return deserializeNode(parentNode, str, currentIndex, currentDepth);
	}


	return distance - 1;
}

void BinaryTreeSerializer::test()
{
	TreeNode* root = new TreeNode(3);
	TreeNode* originalRoot = root;
	{
		auto leftNode = new TreeNode(2);
		root->left = leftNode;
		auto rightNode = new TreeNode(4);
		root->right = rightNode;
		root = leftNode;
	}

	{
		auto leftNode = new TreeNode(1);
		root->left = leftNode;
	}

	BinaryTreeSerializer serializer;
	auto originalStr = serializer.serialize(originalRoot);
	auto newNode = serializer.deserialize(originalStr);
	auto newStr = serializer.serialize(newNode);

	assert(originalStr == newStr);
}