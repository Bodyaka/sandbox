#pragma once

#include <vector>
#include "IProblem.h"

class TriangleProblem
	: public IProblem
{
public:
	TriangleProblem(const std::vector<std::vector<int>>& in);

	void solve() override;

	int getResult() const;

private:
	int m_result;
	std::vector<std::vector<int>> m_in;
};