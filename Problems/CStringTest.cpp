#include "CStringTest.h"
#include <cstring>
#include <iostream>

void CStringTest::solve()
{
	int * arr = new int[100];

	memset(arr, 41, sizeof(int)* 20);
	memset(arr + 20, 1, sizeof(int)* 50);
	//memset(arr + 50, 100, sizeof(int)* 50);

	std::cout << "Printing contents of the array: \n";
	for (int i = 0; i < 100; ++i)
		std::cout << "Value [" << i << "] = " << arr[i] << "\n";


	char ch[] = "Ant man hasrjfd";
	memmove(ch + 8, ch + 4, 5);
	char* charik = new char[sizeof(ch)];
	memcpy(charik, ch, sizeof(ch));

	std::cout << "The ch is = " << ch << "\n";
	std::cout << "The charik is = " << charik << " The size of charik is = " << sizeof(ch) << " Len of charik = " << strlen(charik) << " \n";


}
