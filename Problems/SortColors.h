#pragma once

#include <vector>
#include "IProblem.h"

class SortColors
	: public IProblem
{
public:
	SortColors(std::vector<int>& i_nums);

	static void test();

	virtual void solve() override;

private:
	std::vector<int>& nums;

};