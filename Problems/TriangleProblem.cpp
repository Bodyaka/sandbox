#include "TriangleProblem.h"
#include <algorithm>


TriangleProblem::TriangleProblem(const std::vector<std::vector<int>>& in)
	: m_in(in)
	, m_result(0)
{

}

int TriangleProblem::getResult() const
{
	return m_result;
}




void TriangleProblem::solve()
{

	m_result = 0;

	std::vector<std::vector<int>> input = m_in;

	if (input.empty())
		return;

	int maxColumnSize = input[m_in.size() - 1].size();
	std::vector<int> aux[2];
	aux[0] = std::vector<int>(maxColumnSize);
	aux[1] = std::vector<int>(maxColumnSize);
	
	int previousAuxLevel = 0;
	int currentAuxLevel = 1;

	aux[currentAuxLevel][0] = input[0][0];

	for (int row = 1; row < input.size(); ++row)
	{
		std::swap(previousAuxLevel, currentAuxLevel);

		int columnSize = input[row].size();
		
		for (int column = 0; column < columnSize; ++column)
		{
			int value = input[row][column];
			
			int newPath = 0;
			if (column == 0)
				newPath = aux[previousAuxLevel][column] + value;
			else if (column == columnSize - 1)
				newPath = aux[previousAuxLevel][column - 1] + value;
			else 
			{
				int leftParent = column - 1;
				int rightParent = column;

				int lowerSum = std::min(aux[previousAuxLevel][leftParent], aux[previousAuxLevel][rightParent]);
				newPath = lowerSum + value;
			}

			aux[currentAuxLevel][column] = newPath;
		}	

	}

	int totalPath = std::numeric_limits<int>::max();
	for (int column = 0; column < maxColumnSize; ++column)
	{
		int path = aux[currentAuxLevel][column];
		if (path < totalPath)
			totalPath = path;
	}
	
	m_result = totalPath;
}
