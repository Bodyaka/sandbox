#include "Quicksort4.h"

#include <cassert>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <algorithm>

namespace
{
	
}

Quicksort4::Quicksort4()
{

}

void Quicksort4::solve()
{
	/*
	int N = 0;
	std::cin >> N;
	int* arr = new int[N];
	for (int i = 0; i < N; ++i)
	{
		std::cin >> arr[i];
	}
	std::vector<int> sourceVec(arr, arr + N);
	*/
		std::vector<int> sourceVec = { 1,3,4,5,2,7,8,9,6,10 };
	int insertionSortShifts = insertionSort(sourceVec);

	int numberOfSwaps = quickSort(sourceVec);
	int result = insertionSortShifts - numberOfSwaps;
	assert(result == -16);
	std::cout << result;
}

int Quicksort4::insertionSort(const std::vector<int>& sourceVec) const
{
	int insertionSortShifts = 0;
	std::vector<int> tempVec = sourceVec;
	for (int i = 1; i < tempVec.size(); ++i)
	{
		for (int j = i; j >= 1; --j)
		{
			if (tempVec[j] < tempVec[j - 1])
			{
				++insertionSortShifts;
				std::swap(tempVec[j], tempVec[j - 1]);
			}
		}
	}

	return insertionSortShifts;
}

void Quicksort4::test()
{
	Quicksort4().solve();
}

int Quicksort4::quickSort(const std::vector<int>& sourceVec) const
{
	int numberOfSwaps = 0;
	std::vector<int> sortedVec = sourceVec;
	quickSortHelper(sortedVec, 0, sourceVec.size() - 1, numberOfSwaps);

	return numberOfSwaps;
}

void Quicksort4::quickSortHelper(std::vector<int>& vec, int lo, int high, int& numberOfSwaps) const
{
	if (lo >= high)
		return;

	int partion = quickSortPartition(vec, lo, high, numberOfSwaps);
	quickSortHelper(vec, lo, partion - 1, numberOfSwaps);
	quickSortHelper(vec, partion + 1, high, numberOfSwaps);
}

int Quicksort4::quickSortPartition(std::vector<int>& vec, int lo, int high, int& numberOfSwaps) const
{
	int pivotElement = vec[high];

	int staticIndex = lo;
	for (int i = lo; i < high; ++i)
	{
		int movingIndex = i;
		if (vec[movingIndex] <= pivotElement)
		{
			std::swap(vec[movingIndex], vec[staticIndex]);
			++staticIndex;
			++numberOfSwaps;
		}
	}

	std::swap(vec[staticIndex], vec[high]);
	++numberOfSwaps;
	
	return staticIndex;
}
