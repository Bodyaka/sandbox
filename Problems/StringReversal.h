#pragma once

#include "IProblem.h"
#include <string>

class StringReversal
	: public IProblem
{
public:
	StringReversal();
	StringReversal(const std::string& str);

	void solve() override;

private:
	std::string m_str;
};