#include "LargestContiguousSubsequence.h"
#include <cassert>
#include <algorithm>
#include <limits>

//#define RECURSIVE_SOLVER

void LargestContiguousSubsequence::test()
{
	LargestContiguousSubsequence solver;

	{
		int input[] = { -2, 11, -4, 13, -5, 2 };
		int solution = solver.solve(input, 6);
		assert(solution == 20);
	}

	{
		int input[] = { 1, -3, 4, -2, -1, 6 };
		int solution = solver.solve(input, 6);
		assert(solution == 7);
	}
}

int LargestContiguousSubsequence::solve(int* input, int N)
{
#ifdef RECURSIVE_SOLVER
	int maxValue = std::numeric_limits<int>::min();
	recursiveSolve(input, N, 0, maxValue);
	return maxValue;
#else
	resetSavedValues(N);

	int maxValue = std::numeric_limits<int>::min();
	dynamicProgrammingSolve(input, N, N - 1, maxValue);
	return maxValue;
#endif
}

int LargestContiguousSubsequence::recursiveSolve(int* input, int N, int i, int& maxValue)
{
	if (i >= N)
		return 0;

	int value = std::max(input[i], input[i] + recursiveSolve(input, N, i + 1, maxValue));

	if (value > maxValue)
		maxValue = value;

	return value;
}

void LargestContiguousSubsequence::resetSavedValues(int N)
{
	m_savedValues.swap(std::vector<int>(N, INVALID_VALUE));
}

int LargestContiguousSubsequence::dynamicProgrammingSolve(int* input, int N, int i, int& maxValue)
{
	if (i < 0)
		return 0;

	if (m_savedValues[i] != INVALID_VALUE)
		return m_savedValues[i];
	else
	{
		int value = std::max(input[i], input[i] + dynamicProgrammingSolve(input, N, i - 1, maxValue));
		m_savedValues[i] = value;
		if (value > maxValue)
			maxValue = value;

		return value;
	}
}
