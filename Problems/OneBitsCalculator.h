#pragma once

#include "IProblem.h"

class OneBitsCalculator
	: public IProblem
{
public:

	explicit OneBitsCalculator(int value);

	void solve() override;
private:
	int m_value;
};

