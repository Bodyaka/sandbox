#pragma once

#include <vector>
#include <string>

#include "IProblem.h"

class ValidParenthesis
	: public IProblem
{
public:
	static void test();
	virtual void solve();

private:
	std::vector<std::string> generateParenthesis(int n);
	void backtraceFunc(std::vector<std::string>& validStrings, std::string& currentString, int openBrackets, int closedBrackets, int currentIndex, const int n);
};