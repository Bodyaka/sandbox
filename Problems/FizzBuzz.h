#pragma once

#include "IProblem.h"

class FizzBuzz
	: public IProblem
{
public:
	explicit FizzBuzz(int from = 0, int to = 100);

	void solve() override;

private:
	int m_from,
		m_to;

};