#include "UniqueDigitsProblem.h"
#include <cmath>
#include <cassert>

int UniqueDigitsProblem::solve(int power)
{
	int uniqueDigits = 1;

	int digitOffsets[] = 
	{
		0,
		1,
		3,
		5,
		8,
		11,
		14,
		17,
		20,
		24
	};

	int N = pow(10, power);
	int i = 1;
	while (i < N)
	{
		unsigned int isCorrect = 0;

		int currentNumber = i;
		while (currentNumber != 0)
		{
			unsigned int digit = currentNumber % 10;
			unsigned int offset = digitOffsets[digit];
			if (digit == 0)
				digit = 1;

			digit = digit << offset;
			unsigned int boom = digit | isCorrect;
			if (boom == isCorrect)
			{
				int toSkipPower = 0;
				int toSkipValue = currentNumber / 10;
				while (toSkipValue != 0)
				{
					toSkipValue = toSkipValue / 10;
					++toSkipPower;
				}
				i += pow(10, toSkipPower);
				break;
			}

			isCorrect |= digit;
			currentNumber = currentNumber / 10;

			if (currentNumber == 0)
			{
				++uniqueDigits;
				++i;
			}
		}

	}

	return uniqueDigits;
}

void UniqueDigitsProblem::test()
{
	int digits = solve(3);
	assert(digits == 91);
}
