#pragma once

#include <map>
#include <vector>
#include <string>
#include "IProblem.h"

class LadderItem;

class WorldLadder
	: public IProblem
{
public:
	WorldLadder(const std::string& startWord, const std::string& endWord, const std::vector<std::string>& possibleWords);
	~WorldLadder();

	virtual void solve() override;

private:

	static const size_t INVALID_INDEX = size_t(-1);
	static const size_t END_WORD_INDEX = INVALID_INDEX;

	class ElementItem
	{
	public:

		ElementItem() = default;

		ElementItem(size_t i_rootIndex, size_t i_ladderSize)
			: rootIndex(i_rootIndex)
			, ladderSize(i_ladderSize)
		{

		}

		size_t rootIndex;
		size_t ladderSize;
	};
	std::map<size_t, ElementItem> m_closestElementsDict;

	std::vector<size_t> getClosestElementsToWord(const std::string& desiredWord, std::vector<size_t>& indices) const;
	const std::string& getString(LadderItem* item) const;
	const std::string& getString(size_t index) const;

	size_t calculatePermutations(const std::string& word1, const std::string& word2) const;

	std::string m_startWord,
				m_endWord;

	std::vector<std::string> m_possibleWords;
};