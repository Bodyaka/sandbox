#include "OneBitsCalculator.h"
#include <cassert>
#include <bitset>
#include <iostream>

OneBitsCalculator::OneBitsCalculator(int value)
	: m_value(value)
{

}

void OneBitsCalculator::solve()
{
	const int ONE = 1;
	int bitsNumber = 0;

	unsigned int tempValue = m_value; // In order to consider the negative values
	do
	{
		if (tempValue & ONE)
			++bitsNumber;
	} while (tempValue = tempValue >> 1);

	std::bitset<sizeof(int) * 8> stdBits(m_value);
	int stdBitsNumber = stdBits.count();

	std::cout << "Bits representation of the value " << m_value << " is : " << stdBits << "\n";
	std::cout << "Custom number of bits = " << bitsNumber << " STL number of bits: " << stdBitsNumber << "\n";
	assert(bitsNumber == stdBitsNumber);
}
