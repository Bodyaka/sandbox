#include "UnsignedIntCalculator.h"
#include <iostream>

void UnsignedIntCalculator::solve()
{
	unsigned int i = 1;
	int numberOfBits = 1;

	while (true)
	{
		i = i << 1;
		if (i == 0)
			break;

		++numberOfBits;
	}

	int realSize = sizeof(unsigned int)* 8;
	std::cout << "Calculated size = " << numberOfBits << " Real size = " << realSize << "\n";
}
