// https://leetcode.com/problems/swap-nodes-in-pairs/
#pragma once

#include <vector>

class SwapNodeInPairs
{
	
public:
	struct ListNode {
	    int val;
	    ListNode *next;
	    ListNode(int x) : val(x), next(NULL) {}
	};

public:

	static void test();
	static ListNode* solveProblem(const std::vector<int>& values);

private:

	static ListNode* solveImpl(ListNode* root);

	static ListNode* createListFromValue(const std::vector<int>& values);
	static std::vector<int> getValuesVectorFromNodeList(ListNode* root);
	

};