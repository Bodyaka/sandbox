#include "StringReversal.h"
#include <iostream>
#include <algorithm>


StringReversal::StringReversal()
{
	while (true)
	{
		std::cout << "Enter the string (q or Q - exit): \n";

		std::getline(std::cin, m_str);

		if (m_str == std::string("q") || m_str == std::string("Q"))
			break;

		solve();
	}
}

StringReversal::StringReversal(const std::string& str)
	: m_str(str)
{

}

void StringReversal::solve()
{
	std::cout << "Before solving: " << m_str << "\n";
	std::reverse(m_str.begin(), m_str.end());

	size_t lastSpacePos = -1;


	auto swapWord = [this](size_t lastSpacePos, size_t currentSpacePos) -> void
	{
		size_t wordStart = lastSpacePos;
		size_t wordEnd = currentSpacePos;

		while (true)
		{
			++wordStart;
			--wordEnd;

			if (wordEnd <= wordStart)
				break;

			char tmp = m_str[wordStart];
			m_str[wordStart] = m_str[wordEnd];
			m_str[wordEnd] = tmp;
		}

		
	};

	for (size_t i = 0; i < m_str.size(); ++i)
	{
		char ch = m_str[i];

		if (i == m_str.size() - 1)
			swapWord(lastSpacePos, i + 1);
		else if (ch == ' ')
		{
			swapWord(lastSpacePos, i);

			lastSpacePos = i;
		}
	}

	std::cout << "Final result = " << m_str << "\n";

}
