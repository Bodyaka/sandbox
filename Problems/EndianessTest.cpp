#include "EndianessTest.h"

#include <iostream>

EndianessTest::EndianessTest()
{

}

void EndianessTest::solve()
{
	union EndianessHelper
	{
		int value;
		char ch[sizeof(int)];
	};

	std::cout << "Size of the Endianess Helper Struct: " << sizeof(EndianessHelper) << "\n";

	EndianessHelper helper;
	helper.value = 1;

	if (helper.ch[0] == 1)
		std::cout << "This machines is Little-Endian; \n";
	else if (helper.ch[sizeof(int)-1] == 1)
		std::cout << "This machines is Big-Endian; \n";
	else
		std::cout << "WTF!\n";
}
