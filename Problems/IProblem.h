#pragma once

#include <iostream>
#include <cassert>

inline bool verifyStatement(bool statement, const std::string& problemName, const std::string& sErrorMessage)
{
	if (!statement)
	{
#ifdef _DEBUG
		assert(false && sErrorMessage.c_str());
#else
		std::cout << "ERROR at problem: " << problemName << ": " << sErrorMessage << "!\n";
#endif
	}

	return statement;
}

#define CREATE_TEST_SUITE(classname) static void test();

class IProblem
{
public:
	IProblem() {};
	virtual ~IProblem() {};

	virtual void solve() = 0;
};