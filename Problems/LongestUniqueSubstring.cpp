#include "LongestUniqueSubstring.h"

LongestUniqueSubstring::LongestUniqueSubstring(const std::string& str)
	: m_str(str)
{

}

void LongestUniqueSubstring::test()
{

	{
		LongestUniqueSubstring test("abcabcbb");
		test.solve();
		verifyStatement(test.getResult() == "abc", "LongestUniqueSubstring", "1-st test is failed!");
	}

	{
		LongestUniqueSubstring test("bbbbb");
		test.solve();
		verifyStatement(test.getResult() == "b", "LongestUniqueSubstring", "2-nd test is failed!");
	}

	{
		LongestUniqueSubstring test("pwwkew");
		test.solve();
		verifyStatement(test.getResult() == "wke", "LongestUniqueSubstring", "3-rd test is failed!");
	}
}

void LongestUniqueSubstring::solve()
{
	if (m_str.empty())
		return;

	int iMaxStartIndex = 0;
	int iMaxSize = 1;

	const int ASCII_alphabet_size = 'z' - 'a' + 1;
	char is_repeated[ASCII_alphabet_size];

	int iCurrentStartIndex = 0;
	for (int i = 1; i < m_str.size(); ++i) {
		if (m_str[i] == m_str[i - 1]) {
			int size = i - iCurrentStartIndex;
			if (size > iMaxSize) {
				iMaxStartIndex = iCurrentStartIndex;
				iMaxSize = size;
			}
			
			iCurrentStartIndex = i;
		}
	}

	m_result = m_str.substr(iMaxStartIndex, iMaxSize);
}
