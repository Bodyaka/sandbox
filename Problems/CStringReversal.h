#pragma once

#include "IProblem.h"

class CStringReversal
	: public IProblem
{
public:
	CStringReversal(char* cstr);

	void solve() override;



private:
	char* m_cstr;
};