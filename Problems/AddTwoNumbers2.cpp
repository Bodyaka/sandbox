#include "AddTwoNumbers2.h"

AddTwoNumbers2::AddTwoNumbers2(ListNode* l1, ListNode* l2)
	: m_list1(l1)
	, m_list2(l2)
{

}

AddTwoNumbers2::ListNode* AddTwoNumbers2::addTwoNumbers(ListNode* l1, ListNode* l2) {
	auto i1 = convertListNodeToInt(l1);
	auto i2 = convertListNodeToInt(l2);

	long_t result = i1 + i2;
	return convertIntToListNode(result);
}


AddTwoNumbers2::long_t AddTwoNumbers2::convertListNodeToInt(ListNode* l)
{
	long_t result = 0;
	while (l != nullptr) {
		result *= 10;
		result += l->val;
		l = l->next;
	}

	return result;
}


AddTwoNumbers2::ListNode* AddTwoNumbers2::convertIntToListNode(long_t value)
{
	if (value < 0)
		return nullptr;

	ListNode* prevNode = nullptr;
	do {
		int lastDigit = value % 10;
		ListNode* listNode = new ListNode(lastDigit);
		listNode->next = prevNode;

		prevNode = listNode;
		value = value / 10;
	} while (value != 0);

	return prevNode;
}

void AddTwoNumbers2::solve()
{
	m_result = addTwoNumbers(m_list1, m_list2);
}

AddTwoNumbers2::ListNode* AddTwoNumbers2::getResult() const
{
	std::string s;
	signed char ch = -10;
	s += ch;
	return m_result;
}
