#include "CharFrequencySorter.h"
#include <algorithm>

CharFrequencySorter::CharFrequencySorter(const std::string& i_s)
	: m_string(i_s)
{

}

void CharFrequencySorter::solve()
{
	mySolution();
}

void CharFrequencySorter::mySolution()
{
	const int MAX_ASCII = 256;
	typedef std::pair<int, char> FrequencyPair;

	// Create frequencies list:
	FrequencyPair frequencies[MAX_ASCII];

	for (int i = 0; i < MAX_ASCII; ++i) {
		frequencies[i].first = 0;
		frequencies[i].second = i;
	}

	// Compute frequencies:
	for (char ch : m_string) {
		++frequencies[ch].first;
	}

	// Sort frequencies:
	std::sort(std::begin(frequencies), std::end(frequencies), [](const FrequencyPair& frequencyLeft, const FrequencyPair& frequencyRight) -> bool
	{
		return  frequencyLeft.first > frequencyRight.first;
	});


	// Construct string based on the frequencies:
	for (int i = 0; i < MAX_ASCII; ++i) {
		int n = frequencies[i].first;
		char ch = frequencies[i].second;
		if (n == 0)
			break;

		m_resultString += std::string(n, ch);
	}
}

void CharFrequencySorter::hashBacketSolution()
{

}
