#include "SearchInsertPosition.h"

SearchInsertPosition::SearchInsertPosition(const std::vector<int>& vec, int iNumber)
	: m_vec(vec)
	, m_iNumber(iNumber)
	, m_iResult(-1)
{
	solve();
}

void SearchInsertPosition::solve()
{
	int iStart = 0;
	int iEnd = m_vec.size() - 1;

	while (iStart <= iEnd) 
	{
		int iHalfValue = iStart + (iEnd - iStart) / 2;
		int value = m_vec[iHalfValue];
		if (value == m_iNumber) {
			m_iResult = iHalfValue;
			return;
		}
		else if (m_iNumber > value) {
			iStart = iHalfValue + 1;
		}
		else if (m_iNumber < value) {
			iEnd = iHalfValue - 1;
		}
	}

	// If we went there it means that the value cannot be found!
	if (iEnd < iStart) {
		std::swap(iStart, iEnd);
	}

	m_iResult = iStart + 1;
}

void SearchInsertPosition::test()
{
	assert(SearchInsertPosition({ 1, 3, 5, 6 }, 5).getResult() == 2);
	assert(SearchInsertPosition({ 1, 3, 5, 6 }, 2).getResult() == 1);
	assert(SearchInsertPosition({ 1, 3, 5, 6 }, 7).getResult() == 4);
	assert(SearchInsertPosition({ 1, 3, 5, 6 }, 0).getResult() == 0);
	
	// Better tests:
	assert(SearchInsertPosition({ 1, 3, 9, 11, 16 }, 8).getResult() == 2);
	assert(SearchInsertPosition({ 1, 3, 9, 11, 16 }, 1).getResult() == 0);
	assert(SearchInsertPosition({ 1, 3, 9, 11, 16 }, 16).getResult() == 4);
	assert(SearchInsertPosition({ 1, 3, 9, 11, 16 }, 15).getResult() == 4);
	assert(SearchInsertPosition({ 1, 3, 9, 11, 16 }, 0).getResult() == 0);
	assert(SearchInsertPosition({ 1, 3, 9, 11, 16 }, 100).getResult() == 5);
	assert(SearchInsertPosition({ 1, 3, 9, 11, 16 }, 10).getResult() == 3);
	assert(SearchInsertPosition({ 1, 3, 9, 11, 16 }, 2).getResult() == 1);


	assert(SearchInsertPosition({  }, 10).getResult() == 0);
	assert(SearchInsertPosition({ 1 }, 4).getResult() == 1);
	assert(SearchInsertPosition({ 1 }, 0).getResult() == 0);
}
