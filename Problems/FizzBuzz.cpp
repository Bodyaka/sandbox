#include "FizzBuzz.h"
#include <iostream>
#include <string>

FizzBuzz::FizzBuzz(int from /* = 0 */, int to /* = 100 */)
	: IProblem()
	, m_from(from)
	, m_to(to)
{

}

void FizzBuzz::solve()
{
	std::cout << "Start solving the Fizz Buzz problem: \n";
	for (int i = m_from; i <= m_to; ++i)
	{
		std::string str;
		if (i % 3 == 0)
			str += "Fizz";
		if (i % 5 == 0)
			str += "Buzz";

		if (str.empty())
			str = std::to_string(i);
		
		std::cout << str << " ";
	}
	std::cout << "Fizz Buzz is solved! \n";
}
