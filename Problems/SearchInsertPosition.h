#pragma once

#include <vector>
#include "IProblem.h"

class SearchInsertPosition : public IProblem
{
public:
	SearchInsertPosition(const std::vector<int>& vec, int iNumber);

	int getResult() const { return m_iResult; }
	void solve() override;

	static void test();

private:

	std::vector<int> m_vec;
	const int m_iNumber;

	int m_iResult;
};