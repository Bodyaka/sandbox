#pragma once

#include <vector>
#include <mutex>
#include <thread>
#include "IProblem.h"

class MultipleThreadsJoin
	: public IProblem
{
public:
	MultipleThreadsJoin(int operationsCount = 10, int threadsNumber = 3);

	void solve() override;

	bool isSolved() const;

private:

	void performCalculations(int ownId);
	int m_finishedThreads;
	int m_operationsCount;
	int m_threadsNumber;
	std::mutex m_writeMutex;
	std::mutex m_finalResultMutex;
	std::vector<std::thread> m_threads;
};