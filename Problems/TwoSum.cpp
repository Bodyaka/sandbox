#include "TwoSum.h"
#include <algorithm>

TwoSum::TwoSum(const std::vector<int> & numbers, int desiredNumber)
: m_numbers(numbers)
, m_desiredNumber(desiredNumber)
{

}

void TwoSum::solve()
{
	if (m_numbers.empty())
		return;

	std::vector<int> numbersCopy = m_numbers;

	std::sort(numbersCopy.begin(), numbersCopy.end());

	// O(N^2)
	int currentIndex = 0;
	int upperBound = numbersCopy.size();

	while (currentIndex < upperBound)
	{
		int value1 = numbersCopy[currentIndex];

		for (int i = 0; i < upperBound; ++i)
		{
			if (currentIndex == i)
				continue;

			int value2 = numbersCopy[i];
			if (value2 > m_desiredNumber && i < upperBound)
				upperBound = i;

			if (value1 + value2 == m_desiredNumber)
			{
				int index1 = std::find(m_numbers.begin(), m_numbers.end(), value1) - m_numbers.begin();
				int index2 = std::find(m_numbers.begin(), m_numbers.end(), value2) - m_numbers.begin();

				while (index1 == index2)
					index2 = std::find(m_numbers.begin() + index2 + 1, m_numbers.end(), value2) - m_numbers.begin();

				int newIndex1 = std::min(index1, index2) + 1;
				int newIndex2 = std::max(index1, index2) + 1;

				std::cout << " Found desired number: " << newIndex1 << " " << newIndex2 << "\n";
				return;
			}
		}

		++currentIndex;
	}

}
