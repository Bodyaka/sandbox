#include "ArithmeticRangeOverflowCheck.h"
#include <iostream>


ArithmeticRangeOverflowCheck::ArithmeticRangeOverflowCheck(int number1, int number2)
	: m_number1(number1)
	, m_number2(number2)
{

}

void ArithmeticRangeOverflowCheck::solve()
{
	int mask = 0x80000000;

	bool mask1 = (m_number1 & mask) != 0;
	bool mask2 = (m_number2 & mask) != 0;

	int result = m_number1 + m_number2;

	if (mask1 == mask2)
	{
		bool mask3 = (result & mask) != 0;

		if (mask3 != mask1)
			std::cout << "Overflow of sum between " << m_number1 << " And " << m_number2 << " has occurred! \n";
	}
	else
	{
		std::cout << "Sum of values: " << m_number1 << " and " << m_number2 << " is correct = " << result << "\n";
	}
}
