#pragma once
#include <vector>
#include "IProblem.h"

// LeetCode problem# 26
class SortedArrayDuplicates
	: public IProblem
{
public:
	SortedArrayDuplicates(std::vector<int>& numbers);

	static void defaultTest();

	void solve() override;

private:
	std::vector<int> m_numbers;
};