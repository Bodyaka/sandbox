#include "CStringReversal.h"
#include <cstring>
#include <algorithm>
#include <string>
#include <iostream>

CStringReversal::CStringReversal(char* cstr)
	: m_cstr(cstr)
{

}

void CStringReversal::solve()
{
	if (!m_cstr)
		return;

	std::string toCheckStr(m_cstr);
	std::reverse(toCheckStr.begin(), toCheckStr.end());
	
	int len = strlen(m_cstr);
	int firstIndex = 0, lastIndex = len - 1;

	while (firstIndex < lastIndex)
	{
		char temp = m_cstr[firstIndex];
		m_cstr[firstIndex] = m_cstr[lastIndex];
		m_cstr[lastIndex] = temp;

		++firstIndex;
		--lastIndex;
	}

	std::cout << "Reversed string: " << m_cstr << "\n";
	std::string outStr(m_cstr);
	if (toCheckStr == outStr)
		std::cout << "reversing was done!\n";
	else
		std::cout << "Reversing was failed!\n";

}
