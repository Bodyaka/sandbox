// problem: https://leetcode.com/problems/longest-substring-without-repeating-characters/

#pragma once

#include <string>
#include "IProblem.h"

class LongestUniqueSubstring
	: public IProblem
{
public:

	LongestUniqueSubstring(const std::string& str);
	void solve() override;

	const std::string& getResult() const	{ return m_result; }

	CREATE_TEST_SUITE(LongestUniqueSubstring)

private:
	std::string m_result;
	std::string m_str;
};
