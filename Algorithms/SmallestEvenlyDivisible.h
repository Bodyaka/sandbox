#pragma once

#include <ctime>
#include <list>
#include <algorithm>
#include <iostream>

class SmallestEvenlyDivisible
{
public:
	typedef unsigned long long int LargeIntType;

	SmallestEvenlyDivisible(int from, int to);

	LargeIntType getResult() const;

private:

	void calculateResult();
	void calculateResultFaster();

	int performCalculation(LargeIntType stepValue);

	std::list<int> m_primeNumbers;

	int m_from;
	int m_to;

	LargeIntType m_result;

};

SmallestEvenlyDivisible::SmallestEvenlyDivisible(int from, int to)
	: m_from(from)
	, m_to(to)
	, m_result(-1)
{
	if (m_from > m_to)
		throw std::exception("Incorrect input: to number should be greater than from!");


	int actualFrom = 2;
	int actualTo = m_to;
	
	for (int i = actualFrom; i <= actualTo; ++i)
	{
		bool isPrime = true;
		for (auto primeNumber : m_primeNumbers)
		{
			if (i % primeNumber == 0)
			{
				isPrime = false;
				break;
			}
		}

		if (isPrime)
			m_primeNumbers.push_back(i);
	}

	if (m_primeNumbers.empty())
		throw std::exception("Input range doesn't have any prime numbers!");

	m_primeNumbers.reverse();

	clock_t startTime = clock();
	calculateResultFaster();
	clock_t endTime = clock();
	std::cout << "Result calculation time = " << double(endTime - startTime) / CLOCKS_PER_SEC << "\n";;
}


int SmallestEvenlyDivisible::performCalculation(LargeIntType stepValue)
{
	int actualFrom = std::max(2, m_from);

	LargeIntType number = stepValue;
	while (true)
	{
		bool isResultFound = true;

		for (int valueToTest = m_to; valueToTest != actualFrom; --valueToTest)
		{
			if (number % valueToTest != 0)
				isResultFound = false;
		}

		if (isResultFound)
			break;

		number += stepValue;
	}

	return number;
}


void SmallestEvenlyDivisible::calculateResult()
{
	m_result = performCalculation(m_to);
}

SmallestEvenlyDivisible::LargeIntType SmallestEvenlyDivisible::getResult() const
{
	return m_result;
}

void SmallestEvenlyDivisible::calculateResultFaster()
{
	LargeIntType stepValue = 1;
	for (auto primeNumber : m_primeNumbers)
		stepValue *= primeNumber;

	m_result = performCalculation(stepValue);
}
