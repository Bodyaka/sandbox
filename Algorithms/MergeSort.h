#pragma once

#include <vector>

template <class ElementType>
class MergeSort
{
public:

	typedef std::vector<ElementType> ContainerType;

	MergeSort(ContainerType& container);

	void sort();

private:

	void merge(ContainerType& auxContainer, int start, int median, int end);
	void performSorting(ContainerType& auxContainer, int start, int end);

	ContainerType& m_container;
};

template <class ElementType>
MergeSort<ElementType>::MergeSort(ContainerType& container)
	: m_container(container)
{

}


template <class ElementType>
void MergeSort<ElementType>::sort()
{
	if (m_container.size() <= 1)
		return;

	ContainerType aux = m_container;

	performSorting(aux, 0, m_container.size() - 1);
}

template <class ElementType>
void MergeSort<ElementType>::performSorting(ContainerType& auxContainer, int start, int end)
{
	if (start == end)
		return;

	int medianElement = start + (end - start) / 2;

	performSorting(auxContainer, start, medianElement);
	performSorting(auxContainer, medianElement + 1, end);
	merge(auxContainer, start, medianElement, end);

}


template <class ElementType>
void MergeSort<ElementType>::merge(ContainerType& auxContainer, int start, int median, int end)
{
	int currentIndex = start;
	int firstIndex = start;
	int secondIndex = median + 1;

	while (currentIndex <= end)
	{
		int lowerIndex = 0;
		if (firstIndex == median + 1)
			lowerIndex = secondIndex++;
		else if (secondIndex == end + 1)
			lowerIndex = firstIndex++;
		else
		{
			const ElementType& firstElement = m_container[firstIndex];
			const ElementType& secondElement = m_container[secondIndex];

			if (firstElement <= secondElement)
				lowerIndex = firstIndex++;
			else
				lowerIndex = secondIndex++;
		}


		auxContainer[currentIndex] = m_container[lowerIndex];
		++currentIndex;
	}

	for (int i = start; i <= end; ++i)
		m_container[i] = auxContainer[i];
}
