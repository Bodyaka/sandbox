#pragma once

#include <ctime>
#include <vector>
#include <algorithm>
#include <string>
#include <iostream>

#include "BinarySearch.h"
#include "MergeSort.h"
#include "SmallestEvenlyDivisible.h"

std::vector<int> getRandomVector(int size)
{
	std::vector<int> vec(size);
	srand(time(NULL));
	for (int i = 0; i < size; ++i)
		vec[i] = rand() % 1000;
	
	std::random_shuffle(vec.begin(), vec.end());

	return vec;
}

void binarySearchTest()
{
	std::vector<int> inputContainer = getRandomVector(100);
	std::sort(inputContainer.begin(), inputContainer.end());

	BinarySearch<int> bSearch(inputContainer);

	while (true)
	{
		std::cout << "Enter the value (q or Q - exit): \n";

		std::string str;
		std::cin >> str;

		if (str == std::string("q") || str == std::string("Q"))
			break;

		int value = std::stoi(str);

		bool isFound = bSearch.search(value);


		bool isStdFound = std::binary_search(inputContainer.begin(), inputContainer.end(), value);

		std::cout << "Value = " << value << " Is found by custom: " << isFound << "; Is found by STL: " << isStdFound << "\n";
	}

}

int main()
{
	/*
	std::vector<int> mergeSorted = getRandomVector(100);
	std::vector<int> stdSorted = mergeSorted;

	MergeSort<int> mergeSorter(mergeSorted);
	mergeSorter.sort();

	std::sort(stdSorted.begin(), stdSorted.end());

	if (mergeSorted != stdSorted)
		std::cout << "Merge Sort is incorrect! \n";
	else
		std::cout << "Merge sort is correct! \n";
	*/
		
	SmallestEvenlyDivisible divisible(1, 40);
	std::cout << "Yahooo! The smallest divisible between 1 and 10 = " << divisible.getResult() << "\n";


	system("PAUSE");
	
	return 0;
}