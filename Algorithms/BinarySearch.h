#pragma once

#include <vector>
#include <algorithm>

template <class ElementType>
class BinarySearch
{
public:

	typedef std::vector<ElementType> ContainerType;

	BinarySearch(const ContainerType& container);

	bool search(const ElementType& element);

private:
	ContainerType m_container;

};


template <class ElementType>
BinarySearch<ElementType>::BinarySearch(const ContainerType& container)
	: m_container(container)
{
	std::sort(m_container.begin(), m_container.end());
}

template <class ElementType>
bool BinarySearch<ElementType>::search(const ElementType& element)
{
	if (m_container.empty())
		return false;

	int begin = 0;
	int end = m_container.size();
	int position = -1;

	while (true)
	{
		if (end < begin)
			break;

		int median = begin + (end - begin) / 2;

		const ElementType& medianElement = m_container.at(median);

		if (element < medianElement)
			end = median - 1;
		else if (element > medianElement)
			begin = median + 1;
		else if (element == medianElement)
		{
			position = median;
			return true;
		}
	}

	return false;
}